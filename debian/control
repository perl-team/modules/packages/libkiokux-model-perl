Source: libkiokux-model-perl
Section: perl
Priority: optional
Build-Depends: cdbs,
 devscripts,
 perl,
 debhelper (>= 10~),
 dh-buildinfo,
 libkiokudb-perl,
 libmoosex-strictconstructor-perl,
 libtest-simple-perl (>= 1.001010) | libtest-use-ok-perl | perl (>= 5.21.6)
Maintainer: Debian Perl Group <pkg-perl-maintainers@lists.alioth.debian.org>
Uploaders: Jonas Smedegaard <dr@jones.dk>
Standards-Version: 3.9.4
Vcs-Git: https://salsa.debian.org/perl-team/modules/packages/libkiokux-model-perl.git
Vcs-Browser: https://salsa.debian.org/perl-team/modules/packages/libkiokux-model-perl
Homepage: https://kiokudb.github.io/
Testsuite: autopkgtest-pkg-perl

Package: libkiokux-model-perl
Architecture: all
Depends: ${cdbs:Depends},
 ${misc:Depends},
 ${perl:Depends}
Recommends: ${cdbs:Recommends}
Suggests: ${cdbs:Suggests}
Description: simple application specific wrapper for KiokuDB
 KiokuX::Model is a base class making it easy to create KiokuDB database
 instances in your application. It provides a standard way to
 instantiate and use a KiokuDB object in your apps.
 .
 As your app grows you can subclass it and provide additional
 convenience methods, without changing the structure of the code, but
 simply swapping your subclass for KiokuX::Model in e.g.
 Catalyst::Model::KiokuDB or whatever you use to glue it in.
